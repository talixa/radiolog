package com.talixa.radlog.shared;

public class RadioLogConstants {

	public static final String APP_NAME = "Radio Log";
	public static final String APP_ICON = "res/logs.png";
	public static final String APP_VERSION = "1.0";
	
	public static final String ABOUT_CONTENTS = 
			"<html><center>Application to log heard radio stations."+
			"<br><br>Version: " + APP_VERSION + 
			"<br>Copyright 2017, Talixa Software & Service, LLC</center></html>";
}
