package com.talixa.radlog;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import com.talixa.radlog.listeners.LogEntryUpdatedListener;
import com.talixa.radlog.models.RadioLogDataModel;
import com.talixa.radlog.shared.RadioLogConstants;
import com.talixa.radlog.widgets.JLogEntryPanel;
import com.talixa.swing.SwingApp;
import com.talixa.swing.frames.FrameAbout;
import com.talixa.swing.listeners.ExitActionListener;
import com.talixa.swing.shared.AboutInfo;
import com.talixa.swing.shared.AppInfo;
import com.talixa.swing.shared.IconHelper;

public class RadioLog extends SwingApp {
	
	private static JTable table;
	private static RadioLogDataModel model;
	private static JLogEntryPanel recordEntryPanel;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// create app info bundle
				AppInfo app = getAppInfo();
				
				// create frame
				SwingApp.init(app);
				
				// add gui elements
				createGui();
				
				// start app
				SwingApp.createAndShowGUI(createMenuBar(), createToolBar());
			}
		});	
	}	
	
	private static void createGui() {
		// start with empty dataset
		Object[][] records = {};
		
		// setup data table
		model = new RadioLogDataModel(records);
		table = new JTable(model);
		table.setAutoCreateRowSorter(true);
		updateTableDisplay();
		
		// set on click listener
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent event) {
				int recno = table.getSelectedRow();
				if (recno != -1) {
					@SuppressWarnings("unchecked")
					Vector<Object> row = (Vector<Object>)model.getDataVector().elementAt(recno);
					recordEntryPanel.setFromRecord(row);
				}
			}
	    });
		
		// add to scroll pane and then to frame
		JScrollPane scrollPane = new JScrollPane(table);
		frame.add(scrollPane, BorderLayout.CENTER);
		
		// add panel for data entry
		recordEntryPanel = new JLogEntryPanel();
		recordEntryPanel.setUpdateListener(new LogEntryUpdatedListener() {
			public void onEntryUpdated(Object[] data) {
				int recno = table.getSelectedRow();
				if (recno == -1) {
					model.addRow(data);
				} else {
					for(int i = 0; i < data.length; ++i) {
						model.setValueAt(data[i], recno, i);
					}
				}
				model.fireTableDataChanged();
			}
		});
		frame.add(recordEntryPanel, BorderLayout.SOUTH);
	}
	
	// basic app options
	private static AppInfo getAppInfo() {
		AppInfo appInfo = new AboutInfo();
		appInfo.height = 600;
		appInfo.width = 550;
		appInfo.version = RadioLogConstants.APP_VERSION;
		appInfo.name = RadioLogConstants.APP_NAME;
		appInfo.icon = RadioLogConstants.APP_ICON;
		return appInfo;
	}
	
	private static void updateTableDisplay() {
		// setup renderer for right aligned cells
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
				
		for(int i = 0; i < model.getColumnCount(); ++i) {
			table.getColumnModel().getColumn(i).setPreferredWidth(model.getPreferedWidth(i));
		}
		table.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
	}
	
	// info for the about screen
	private static AboutInfo getAboutInfo() {
		AboutInfo appInfo = new AboutInfo();
		appInfo.height = 210;
		appInfo.width = 300;
		appInfo.version = RadioLogConstants.APP_VERSION;
		appInfo.name = RadioLogConstants.APP_NAME;
		appInfo.icon = RadioLogConstants.APP_ICON;
		appInfo.contents = RadioLogConstants.ABOUT_CONTENTS;
		return appInfo;
	}
	
	// Using images from: http://www.oracle.com/technetwork/java/index-138612.html
	private static JToolBar createToolBar() {
		JToolBar toolbar = new JToolBar();
		toolbar.setFloatable(false);
		
		JButton btnOpen = new JButton(IconHelper.getImageIcon("res/open.gif"));
		btnOpen.setToolTipText("Open existing database");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadFile();
			}
		});
		toolbar.add(btnOpen);
		
		JButton btnSave = new JButton(IconHelper.getImageIcon("res/save.gif"));
		btnSave.setToolTipText("Save database");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFile();
			}
		});
		toolbar.add(btnSave);
		toolbar.addSeparator();
		
		JButton btnAdd = new JButton(IconHelper.getImageIcon("res/add.gif"));
		btnAdd.setToolTipText("Add new record");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recordEntryPanel.createEmptyRecord();
				table.clearSelection();
			}
		});
		toolbar.add(btnAdd);
		
		JButton btnDelete = new JButton(IconHelper.getImageIcon("res/delete.gif"));
		btnDelete.setToolTipText("Delete selected record");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(frame, "Are you sure you want to delete this record?", "Confirm Delete", JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
					if (table.getSelectedRow() != -1) {
						model.removeRow(table.getSelectedRow());
						recordEntryPanel.disableEntry();
					}
				}
			}
		});
		toolbar.add(btnDelete);
		
		return toolbar;
	}
	
	// create the menu bar
	private static JMenuBar createMenuBar() {
		JMenuBar menu = new JMenuBar();
		
		// File menu
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem menuFileExit = new JMenuItem("Exit");
		menuFileExit.setMnemonic(KeyEvent.VK_X);
		menuFileExit.addActionListener(new ExitActionListener(SwingApp.frame));
		menuFile.add(menuFileExit);
		menu.add(menuFile);
		
		// Help menu
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		
		JMenuItem menuHelpAbout = new JMenuItem("About");
		menuHelpAbout.setMnemonic(KeyEvent.VK_A);
		menuHelpAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(SwingApp.frame, getAboutInfo());
			}
		});
		menuHelp.add(menuHelpAbout);
		menu.add(menuHelp);
		
		return menu;
	}
	
	private static void saveFile() {
		if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
			ObjectOutputStream oos = null;
			try {
				oos = new ObjectOutputStream(new FileOutputStream(fileChooser.getSelectedFile()));
				oos.writeObject(model.getDataVector());
			} catch (Exception ex) {
				showErrorMessage("Unable to save log");
			} finally {
				if (oos != null) {
					try {
						oos.close();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static void loadFile() {
		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			ObjectInputStream ois = null;
			try {
				ois = new ObjectInputStream(new FileInputStream(fileChooser.getSelectedFile()));
				model.setDataVector((Vector)ois.readObject(), RadioLogDataModel.getHeaders());
				updateTableDisplay();
			} catch (Exception ex) {
				showErrorMessage("Unable to open log");
			} finally {
				if (ois != null) {
					try {
						ois.close();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
}
