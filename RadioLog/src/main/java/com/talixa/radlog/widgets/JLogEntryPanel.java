package com.talixa.radlog.widgets;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.radlog.listeners.LogEntryUpdatedListener;

@SuppressWarnings("serial")
public class JLogEntryPanel extends JPanel {

	private LogEntryUpdatedListener listener;
	
	private JLabel lblDate;
	private JTextField txtDate;
	private JLabel lblTime;
	private JTextField txtTime;
	private JLabel lblFreq;
	private JTextField txtFreq;
	private JLabel lblMode;
	private JTextField txtMode;
	private JLabel lblStr;
	private JTextField txtStr;
	private JLabel lblCall;
	private JTextField txtCall;
	private JLabel lblUser;
	private JTextField txtUser;
	private JLabel lblComments;
	private JTextField txtComments;
	
	private JButton btnSave;
	
	public JLogEntryPanel() {
		super(new GridLayout(4, 1));
		
		txtDate = new JTextField(8);
		txtTime = new JTextField(6);
		txtFreq = new JFormattedTextField(NumberFormat.getIntegerInstance());
		txtFreq.setColumns(6);
		txtMode = new JTextField(6);
		txtStr = new JTextField(4);
		txtCall = new JTextField(12);
		txtUser = new JTextField(12);
		txtComments = new JTextField(30);
		
		lblDate = new JLabel("Date");
		lblTime = new JLabel("Time");
		lblFreq = new JLabel("Freq");
		lblMode = new JLabel("Mode");
		lblStr = new JLabel("Str");
		lblCall = new JLabel("Call");
		lblUser = new JLabel("User");
		lblComments = new JLabel("Comments");
		

		// first panel
		JPanel row1 = new JPanel(new FlowLayout());
		row1.add(lblDate);
		row1.add(txtDate);
		row1.add(lblTime);
		row1.add(txtTime);
		row1.add(lblFreq);
		row1.add(txtFreq);
		row1.add(lblMode);
		row1.add(txtMode);
		
		JPanel row2 = new JPanel(new FlowLayout());
		row2.add(lblCall);
		row2.add(txtCall);
		row2.add(lblUser);
		row2.add(txtUser);
		row2.add(lblStr);
		row2.add(txtStr);
		
		JPanel row3 = new JPanel(new FlowLayout());
		row3.add(lblComments);
		row3.add(txtComments);
			
		this.add(row1);
		this.add(row2);
		this.add(row3);
		
		// set save button
		btnSave = new JButton("Update Record");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listener != null) {
					String date = txtDate.getText();
					String time = txtTime.getText();
					String call = txtCall.getText();
					String user = txtUser.getText();
					String mode = txtMode.getText();
					int freq = Integer.valueOf(txtFreq.getText());
					String str = txtStr.getText();
					String comments = txtComments.getText();
					listener.onEntryUpdated(new Object[] {date,time,freq,mode,str,call,user,comments});
				}
			}
		});
		btnSave.setEnabled(false);
		
		JPanel row4 = new JPanel(new FlowLayout());
		row4.add(btnSave);
		this.add(row4);
	}
	
	public void setFromRecord(Vector<Object> data) {
		this.txtDate.setText((String)data.elementAt(0));
		this.txtTime.setText((String)data.elementAt(1));
		this.txtFreq.setText(((Integer)data.elementAt(2)).toString());
		this.txtMode.setText((String)data.elementAt(3));
		this.txtStr.setText((String)data.elementAt(4));
		this.txtCall.setText((String)data.elementAt(5));
		this.txtUser.setText((String)data.elementAt(6));
		this.txtComments.setText((String)data.elementAt(7));
		btnSave.setText("Update Record");
		btnSave.setEnabled(true);
	}
	
	public void createEmptyRecord() {
		clearData();
		this.btnSave.setText("Add Record");
		btnSave.setEnabled(true);
	}
	
	public void disableEntry() {
		clearData();
		this.btnSave.setText("Update Record");
		btnSave.setEnabled(false);
	}
	
	private void clearData() {
		this.txtComments.setText("");
		this.txtCall.setText("");
		this.txtDate.setText("");
		this.txtFreq.setText("");
		this.txtMode.setText("");
		this.txtStr.setText("");
		this.txtTime.setText("");
		this.txtUser.setText("");
	}
	
	public void setUpdateListener(LogEntryUpdatedListener l) {
		this.listener = l;
	}
}
