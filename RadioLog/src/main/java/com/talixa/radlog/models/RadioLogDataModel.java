package com.talixa.radlog.models;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class RadioLogDataModel extends DefaultTableModel {
	
	private static final String HEADERS[] = {"Date","Time","Freq","Mode","Str","Call","User","Comments"};

	public RadioLogDataModel(Object[][] records) {
		for(String h : HEADERS) {
			addColumn(h);
		}
	
		for(int i = 0; i < records.length; ++i) {
			this.addRow(records[i]);
		}
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 2) {
			return Integer.class;
		} else {
			return String.class;
		}
	}


	public int getPreferedWidth(int col) {
		switch (col) {
			case 0: return 120;
			case 1: return 50;
			case 2: return 70;
			case 3: return 90;
			case 4: return 35;
			case 5: return 100;
			case 6: return 120;
			case 7: return 200;
		}
		return 500;
	}
	
	public static Vector<String> getHeaders() {
		return new Vector<String>(Arrays.asList(HEADERS));
	}
}
