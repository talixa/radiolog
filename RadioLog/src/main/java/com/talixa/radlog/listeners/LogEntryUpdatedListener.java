package com.talixa.radlog.listeners;

public interface LogEntryUpdatedListener {
	public void onEntryUpdated(Object[] data);
}
