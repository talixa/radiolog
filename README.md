# Radio Log

Radio Log is a simple Java logger for radio signals.

# Dependencies
This project uses [SwingLib](https://bitbucket.org/tcgerlach/swinglib) for creating desktop applications. Please build and install this library before building this application.